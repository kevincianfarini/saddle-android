package com.kevincianfarini.saddle.di

import android.content.Context
import com.kevincianfarini.saddle.SaddleApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: SaddleApplication) {

    @Singleton @Provides
    fun providesApp() = app

    @Singleton @Provides
    fun providesContext() = app as Context
}