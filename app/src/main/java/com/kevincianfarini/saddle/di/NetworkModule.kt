package com.kevincianfarini.saddle.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.kevincianfarini.saddle.data.network.SaddleAPI
import com.kevincianfarini.saddle.feature.preference.AppPreferences
import dagger.Module
import dagger.Provides
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit

@Module
class NetworkModule {

    @Provides
    fun providesSaddleAPI(prefs: AppPreferences): SaddleAPI {
        val contentType = MediaType.get("application/json")
        return Retrofit.Builder()
            .baseUrl(requireNotNull(prefs.serverUrl) { "Server URL must not be null" })
            .addConverterFactory(Json.asConverterFactory(contentType))
            .build()
            .create(SaddleAPI::class.java)
    }
}