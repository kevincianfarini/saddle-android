package com.kevincianfarini.saddle.di

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import javax.inject.Named

@Module
class CoroutinesModule {

    @Provides @Named("Main")
    fun providesMainCoroutineScope() = CoroutineScope(Dispatchers.Main + SupervisorJob())

    @Provides @Named("IO")
    fun providesIOCoroutineScope() = CoroutineScope(Dispatchers.IO + SupervisorJob())
}