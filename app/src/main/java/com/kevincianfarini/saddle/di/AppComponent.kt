package com.kevincianfarini.saddle.di

import com.kevincianfarini.saddle.feature.base.view.BaseFragment
import com.kevincianfarini.saddle.feature.preference.view.PreferenceFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    CoroutinesModule::class,
    NetworkModule::class
])
interface AppComponent {

    fun inject(fragment: BaseFragment)
    fun inject(fragment: PreferenceFragment)
}