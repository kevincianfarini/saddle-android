package com.kevincianfarini.saddle

import android.app.Application
import com.kevincianfarini.saddle.di.AppComponent
import com.kevincianfarini.saddle.di.AppModule
import com.kevincianfarini.saddle.di.DaggerAppComponent

class SaddleApplication : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}