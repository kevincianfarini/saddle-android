package com.kevincianfarini.saddle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.kevincianfarini.saddle.feature.navigation.view.BottomNavigationFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.setFragment(BottomNavigationFragment())
    }

    @Deprecated("To be replaced by navigation framework")
    fun setFragment(fragment: Fragment, addToBackStack: Boolean = false) {
        with(supportFragmentManager.beginTransaction()) {
            this.replace(R.id.content, fragment)
            if (addToBackStack) {
                this.addToBackStack(null)
            }
            this.commit()
        }
    }
}
