package com.kevincianfarini.saddle.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hu.autsoft.krate.Krate
import hu.autsoft.krate.stringPref
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View = LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)

fun Krate.stringPref(key: String, isValid: (String?) -> Boolean): ReadWriteProperty<Krate, String?> {
    return ValidatedPreferenceDelegate(stringPref(key), isValid)
}

/**
 * [ValidatedPreferenceDelegate] is a generic [ReadWriteProperty] that can be used to
 * validate values that are being set.
 *
 * TODO get rid of this after it gets merged to Krate
 *
 * @param [delegate] the [ReadWriteProperty] implementation that is used for delegation
 * @param [isValid] the lambda used to validate property values on [setValue]
 */
internal class ValidatedPreferenceDelegate<T>(
    private val delegate: ReadWriteProperty<Krate, T>,
    private val isValid: (T) -> Boolean
) : ReadWriteProperty<Krate, T> by delegate {

    override operator fun setValue(thisRef: Krate, property: KProperty<*>, value: T) {
        if (!isValid(value)) {
            throw KrateValidationException("$value is not valid.")
        }

        delegate.setValue(thisRef, property, value)
    }
}

class KrateValidationException(message: String) : Exception(message)