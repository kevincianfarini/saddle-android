package com.kevincianfarini.saddle.feature.base.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.annotation.CallSuper
import com.kevincianfarini.saddle.SaddleApplication
import com.kevincianfarini.saddle.di.AppComponent

abstract class BaseFragment : Fragment() {

    abstract val hasMenu: Boolean

    /**
     * We override this method to ensure that we inject this Fragment's
     * required dependencies
     */
    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.injectDependencies((activity?.application as SaddleApplication).appComponent)
        this.setHasOptionsMenu(hasMenu)
    }

    /**
     * All derived classes must implement this function
     * so that we can inject dependencies into the derived
     * Fragment properly
     *
     * @param [component] the [AppComponent] provided from the [SaddleApplication] instance
     */
    abstract fun injectDependencies(component: AppComponent)
}