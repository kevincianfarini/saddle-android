package com.kevincianfarini.saddle.feature.preference

import android.content.Context
import android.webkit.URLUtil
import com.kevincianfarini.saddle.util.stringPref
import hu.autsoft.krate.SimpleKrate
import okhttp3.OkHttpClient
import okhttp3.Request
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppPreferences @Inject constructor(context: Context) : SimpleKrate(context) {

    var serverUrl: String? by stringPref("server_url") {
        requireNotNull(it).let { url ->
            val isHealthy: Boolean by lazy {
                val request = Request.Builder()
                    .url("$url/api/healthcheck")
                    .build()

                try {
                    OkHttpClient().newCall(request).execute().isSuccessful
                } catch (e: Exception) {
                    false
                }
            }

            URLUtil.isValidUrl(it) && isHealthy
        }
    }
}