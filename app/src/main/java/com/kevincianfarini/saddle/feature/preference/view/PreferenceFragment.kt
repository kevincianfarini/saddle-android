package com.kevincianfarini.saddle.feature.preference.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import com.kevincianfarini.saddle.R
import com.kevincianfarini.saddle.di.AppComponent
import com.kevincianfarini.saddle.feature.base.view.BaseFragment
import com.kevincianfarini.saddle.feature.preference.viewmodel.PreferenceViewModel
import com.kevincianfarini.saddle.util.inflate
import kotlinx.android.synthetic.main.fragment_preferences.*
import javax.inject.Inject

class PreferenceFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: PreferenceViewModel
    override val hasMenu = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        container?.inflate(R.layout.fragment_preferences)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstanceAddressEditText()
        initLiveData()
    }

    private fun initLiveData() {
        viewModel.error.observe(this, Observer {
            instanceAddress.error = it.message
        })
    }

    private fun initInstanceAddressEditText() {
        viewModel.getInstanceUrl()?.let {
            instanceAddress.setText(it)
        }

        instanceAddress.setOnEditorActionListener { textView, actionId, _ ->
            if (EditorInfo.IME_ACTION_DONE == actionId) {
                viewModel.setInstanceUrl(textView.text.toString())
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    override fun injectDependencies(component: AppComponent) {
        component.inject(this)
    }
}