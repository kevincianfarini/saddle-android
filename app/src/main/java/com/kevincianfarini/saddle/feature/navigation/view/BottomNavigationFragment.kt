package com.kevincianfarini.saddle.feature.navigation.view

import android.os.Bundle
import android.view.*
import com.kevincianfarini.saddle.MainActivity
import com.kevincianfarini.saddle.R
import com.kevincianfarini.saddle.di.AppComponent
import com.kevincianfarini.saddle.feature.base.view.BaseFragment
import com.kevincianfarini.saddle.feature.preference.view.PreferenceFragment
import com.kevincianfarini.saddle.util.inflate

class BottomNavigationFragment : BaseFragment() {

    override val hasMenu: Boolean = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        container?.inflate(R.layout.fragment_bottom_navigation)

    override fun injectDependencies(component: AppComponent) {
        // TODO implement
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_preferences -> {
            (activity as MainActivity).setFragment(PreferenceFragment(), addToBackStack = true)
            true
        }
        else -> false
    }
}