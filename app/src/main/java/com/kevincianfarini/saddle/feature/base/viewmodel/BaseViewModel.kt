package com.kevincianfarini.saddle.feature.base.viewmodel

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import java.util.concurrent.CancellationException

abstract class BaseViewModel(private val scope: CoroutineScope) : ViewModel(), CoroutineScope by scope {

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        this.cancel(CancellationException("$this called onCleared"))
    }
}