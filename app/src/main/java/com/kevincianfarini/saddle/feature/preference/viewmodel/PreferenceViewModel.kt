package com.kevincianfarini.saddle.feature.preference.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kevincianfarini.saddle.feature.base.viewmodel.BaseViewModel
import com.kevincianfarini.saddle.feature.preference.AppPreferences
import com.kevincianfarini.saddle.util.KrateValidationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

class PreferenceViewModel @Inject constructor(
    private val preferences: AppPreferences,
    @Named("Main") scope: CoroutineScope
) : BaseViewModel(scope) {

    private val _error: MutableLiveData<KrateValidationException> = MutableLiveData()
    val error: LiveData<KrateValidationException> = _error

    // TODO coroutines
    fun getInstanceUrl(): String? = preferences.serverUrl

    fun setInstanceUrl(value: String) = this.launch(Dispatchers.IO) {
        try {
            preferences.serverUrl = value
        } catch (e: KrateValidationException) {
            _error.postValue(e)
        }
    }
}